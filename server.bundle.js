/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"react\"\n// module id = 0\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"react-router\"\n// module id = 1\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22react-router%22?");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"express\"\n// module id = 2\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"path\"\n// module id = 3\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar bluebird = __webpack_require__(11);\nvar connectOptions = {\n    promiseLib: bluebird,\n    capSQL: true\n    // connect: (client, dc, isFresh) => {\n    // const cp = client.connectionParameters;\n    // console.log('Connected to database:', cp.database + ':' + cp.port);\n    // console.log(client);\n    // console.log(isFresh);\n    // },\n};\nvar pgp = __webpack_require__(5)(connectOptions);\n\n// Connection configuration object to msttest pgpool\nvar config = {\n    host: '192.168.1.48',\n    port: '9999',\n    database: 'edb',\n    user: 'enterprisedb',\n    password: '$010209!'\n};\n\nvar productionHTTPS = {\n    host: '192.168.1.40',\n    port: '9999',\n    database: 'edb',\n    user: 'enterprisedb',\n    password: '$010209!'\n};\n\nvar productionConfig = {\n    host: '192.168.1.40',\n    port: '9999',\n    database: 'edb',\n    user: 'enterprisedb',\n    password: '$010209!'\n};\n\n// Connection configuration object to local\nvar config2 = {\n    host: 'localhost',\n    port: '5432',\n    database: 'postgres',\n    user: 'postgres',\n    password: 'josh1470'\n};\n\nvar edbTest = pgp(config);\nvar edb = pgp(productionConfig);\n\nvar MyConn = {\n    pgp: pgp,\n    edbTest: edbTest,\n    edb: edb\n};\n\nmodule.exports = MyConn;\n\n//////////////////\n// WEBPACK FOOTER\n// ./connections/posgres-connection.js\n// module id = 4\n// module chunks = 0\n\n//# sourceURL=webpack:///./connections/posgres-connection.js?");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

eval("module.exports = require(\"pg-promise\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"pg-promise\"\n// module id = 5\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22pg-promise%22?");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("/* WEBPACK VAR INJECTION */(function(__dirname) {\n\nvar _react = __webpack_require__(0);\n\nvar _react2 = _interopRequireDefault(_react);\n\nvar _server = __webpack_require__(7);\n\nvar _reactRouter = __webpack_require__(1);\n\nvar _routes = __webpack_require__(8);\n\nvar _routes2 = _interopRequireDefault(_routes);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n//EXPRESS SERVER\nvar express = __webpack_require__(2);\nvar app = express();\nvar router = express.Router();\nvar path = __webpack_require__(3);\nvar fs = __webpack_require__(9);\n// const React = require('react');\n// we'll use this to render our app to an html string\n\n// const renderToString = require('react-dom/server');\n// and these to match the url to routes and then render\n\n// const match = require('react-router/matchPath');\n// const RouterContext = require('react-router/Route');\n\n// const routes = require('./src/routes');\n\n\n//PARSER\nvar bodyParser = __webpack_require__(10);\napp.use(bodyParser.urlencoded({ extended: true }));\napp.use(bodyParser.json());\n\n//CONNECTIONS\nvar MyConn = __webpack_require__(4);\nMyConn.edb.query('select inet_server_port();').then(function (result) {\n    try {\n        var dbPort = result[0].inet_server_port;\n        console.log('Connected to database on port: ' + dbPort);\n    } catch (err) {\n        console.log(err);\n    }\n}).catch(function (err) {\n    console.log(err);\n});\n\napp.use(express.static(path.join(__dirname, './public')));\n\n//API'S *Require and use API'S here*\n\nvar dataMonsterAPI = __webpack_require__(12);\napp.use('/api', dataMonsterAPI);\n\napp.get('*.js', function (req, res, next) {\n    req.url = req.url + '.gz';\n    res.set('Content-Encoding', 'gzip');\n    next();\n});\n\n// React Router\napp.get('*', function (req, res) {\n    (0, _reactRouter.match)({ routes: _routes2.default, location: req.url }, function (err, redirect, props) {\n\n        // in here we can make some decisions all at once\n        if (err) {\n            // there was an error somewhere during route matching\n            res.status(500).send(err.message);\n        } else if (redirect) {\n            // we haven't talked about `onEnter` hooks on routes, but before a\n            // route is entered, it can redirect. Here we handle on the server.\n            res.redirect(redirect.pathname + redirect.search);\n        } else if (props) {\n            // if we got props then we matched a route and can render\n            var appHtml = (0, _server.renderToString)(_react2.default.createElement(_reactRouter.RouterContext, props));\n            res.send(renderPage(appHtml));\n        } else {\n            // no errors, no redirect, we just didn't match anything\n            res.status(404).send('Not Found');\n        }\n    });\n});\n\nfunction renderPage(appHtml) {\n    return '\\n    <!DOCTYPE html>\\n    <html lang=\"en\">\\n    <head>\\n    <meta charset=\"UTF-8\">\\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\\n    <title>Myers-Stevens & Toohey & Co., Inc.</title>\\n    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">\\n    <link rel=\"stylesheet\" href=\"/index.css\">\\n    </head>\\n    <body>\\n    <div id=app>' + appHtml + '</div>\\n    <script src=\"./bundle.js\"></script>\\n    </body>\\n    </html>\\n    ';\n}\n\n//ROUTER FUNCTIONS\nrouter.use(function (req, res, next) {\n    console.log('Server ' + req.method, req.url, req.body);\n    next();\n});\n\napp.use('/api', router);\n\n//SERVER CONNECTION\nvar port = Object({\"NODE_ENV\":\"production\"}).PORT || 9898;\nvar server = app.listen(Object({\"NODE_ENV\":\"production\"}).PORT || port || 9898, function () {\n    console.log('Server is running at https://localhost:' + server.address().port);\n});\nmodule.exports = app;\n/* WEBPACK VAR INJECTION */}.call(exports, \"\"))\n\n//////////////////\n// WEBPACK FOOTER\n// ./server.js\n// module id = 6\n// module chunks = 0\n\n//# sourceURL=webpack:///./server.js?");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"react-dom/server\"\n// module id = 7\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _react = __webpack_require__(0);\n\nvar _react2 = _interopRequireDefault(_react);\n\nvar _reactRouter = __webpack_require__(1);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/**\n * Created by jwarnock on 2/3/17.\n */\nmodule.exports = _react2.default.createElement(\n    _reactRouter.Router,\n    { path: '/', history: _reactRouter.browserHistory },\n    _react2.default.createElement(_reactRouter.IndexRoute, null)\n);\n\n//////////////////\n// WEBPACK FOOTER\n// ./src/routes.js\n// module id = 8\n// module chunks = 0\n\n//# sourceURL=webpack:///./src/routes.js?");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

eval("module.exports = require(\"fs\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"fs\"\n// module id = 9\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22fs%22?");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"body-parser\"\n// module id = 10\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22body-parser%22?");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

eval("module.exports = require(\"bluebird\");\n\n//////////////////\n// WEBPACK FOOTER\n// external \"bluebird\"\n// module id = 11\n// module chunks = 0\n\n//# sourceURL=webpack:///external_%22bluebird%22?");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/**\n * Created by jwarnock on 7/31/17.\n */\nvar express = __webpack_require__(2);\nvar router = express.Router();\nvar MyConn = __webpack_require__(4);\nvar sql = __webpack_require__(13);\n\nrouter.route('/connecttest').get(function (req, res) {\n\n    res.send('WORKING');\n});\n\n// RVS MEDICAL SERVICE\n// TABLE CREATE\nrouter.route('/medicalservicecreate').post(function (req, res) {\n    MyConn.edb.query(sql.medFile.tableCreate).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Medical Service Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Medical Service Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n// TEST\nrouter.route('/testmedicalservicecreate').post(function (req, res) {\n    MyConn.edbTest.query(sql.medFile.tableCreate).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Medical Service Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Medical Service Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n\n// ZIP ZIPCODE\n// TABLE CREATE\nrouter.route('/zipcodecreate').post(function (req, res) {\n    MyConn.edb.query(sql.zipFile.tableCreate).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Zip Codes Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Zip Codes Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n// TEST\nrouter.route('/testzipcodecreate').post(function (req, res) {\n    MyConn.edbTest.query(sql.zipFile.tableCreate).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Zip Codes Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Zip Codes Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n\n// DATA INSERT\nrouter.route('/insertzipcodes').post(function (req, res) {\n    var zipCodeData = {\n        geoZipCode: req.body.geoZipCode,\n        zipCodeLow: req.body.zipCodeLow,\n        zipCodeHigh: req.body.zipCodeHigh\n    };\n\n    MyConn.edb.query(sql.zipFile.insertZipCodes, zipCodeData).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Zip Codes Inserted'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Zip Codes Not Inserted'\n        };\n        res.send(tableCreateError);\n    });\n});\n// TEST\nrouter.route('/testinsertzipcodes').post(function (req, res) {\n    var zipCodeData = {\n        geoZipCode: req.body.geoZipCode,\n        zipCodeLow: req.body.zipCodeLow,\n        zipCodeHigh: req.body.zipCodeHigh\n    };\n    MyConn.edbTest.query(sql.zipFile.insertZipCodes, zipCodeData).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Zip Codes Inserted'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Zip Codes Not Inserted'\n        };\n        res.send(tableCreateError);\n    });\n});\n\n// FCT GEO FACTOR\n\n// TABLE CREATE\nrouter.route('/geofactorscreate').post(function (req, res) {\n    MyConn.edb.query(sql.geoFile.tableCreate).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Geo Factors Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Geo Factors Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n// TEST\nrouter.route('/testgeofactorscreate').post(function (req, res) {\n    MyConn.edbTest.query(sql.geoFile.tableCreate).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Geo Factors Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Geo Factors Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n\n// INSERT DATA\nrouter.route('/insertgeofactordata').post(function (req, res) {\n    var geoFactorData = {\n        geoZipCode: req.body.data.geoZipCode,\n        procedureCodeLow: req.body.data.procedureCodeLow,\n        procedureCodeHigh: req.body.data.procedureCodeHigh,\n        percentile60in: req.body.data.percentile60in,\n        percentile80in: req.body.data.percentile80in,\n        percentile85in: req.body.data.percentile85in,\n        percentile90in: req.body.data.percentile90in,\n        conversionFactorType: req.body.data.conversionFactorType\n    };\n    MyConn.edb.query(sql.geoFile.insertGeoFactorData, geoFactorData).then(function (result) {\n        var tableCreateResult = {\n            code: 1,\n            result: 'OK',\n            data: result,\n            message: 'Zip Codes Table Created'\n        };\n        res.send(tableCreateResult);\n    }).catch(function (err) {\n        console.log(err);\n        var tableCreateError = {\n            code: 0,\n            result: 'Error',\n            data: err,\n            message: 'Zip Codes Table Not Created'\n        };\n        res.send(tableCreateError);\n    });\n});\n\n// router.route('/insertgeofactordatamass')\n//     .post(function(req, res) {\n//         const geoFactorData = {\n//             geoZipCode: req.body.data.geoZipCode,\n//             procedureCodeLow: req.body.data.procedureCodeLow,\n//             procedureCodeHigh: req.body.data.procedureCodeHigh,\n//             percentile60in: req.body.data.percentile60in,\n//             percentile80in: req.body.data.percentile80in,\n//             percentile85in: req.body.data.percentile85in,\n//             percentile90in: req.body.data.percentile90in,\n//             conversionFactorType: req.body.data.conversionFactorType,\n//         };\n//         const cs = new MyConn.pgp.helpers.ColumnSet([\n//             {name: 'geo_zip_code', prop: 'geoZipCode'},\n//             {name: 'procedure_code_low', prop: 'procedureCodeLow'},\n//             {name: 'procedure_code_high', prop: 'procedureCodeHigh'},\n//             {name: 'percentile_60_in', prop: 'percentile60in'},\n//             {name: 'percentile_80_in', prop: 'percentile80in'},\n//             {name: 'percentile_85_in', prop: 'percentile85in'},\n//             {name: 'percentile_90_in', prop: 'percentile90in'},\n//             {name: 'conversion_factor_type', prop: 'conversionFactorType'},\n//         ]);\n//         edb.query(sql.geoFile.insertGeoFactorData, geoFactorData)\n//             .then(result => {\n//                 // console.log(result);\n//                 const tableCreateResult = {\n//                     code: 1,\n//                     result: 'OK',\n//                     data: result,\n//                     message: 'Zip Codes Table Created',\n//                 };\n//                 res.send(tableCreateResult);\n//             })\n//             .catch(err => {\n//                 console.log(err);\n//                 const tableCreateError = {\n//                     code: 0,\n//                     result: 'Error',\n//                     data: err,\n//                     message: 'Zip Codes Table Not Created',\n//                 };\n//                 res.send(tableCreateError);\n//             })\n//     });\n\n\nmodule.exports = router;\n\n//////////////////\n// WEBPACK FOOTER\n// ./api/datamonster-api.js\n// module id = 12\n// module chunks = 0\n\n//# sourceURL=webpack:///./api/datamonster-api.js?");

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("/* WEBPACK VAR INJECTION */(function(__dirname) {\n\nvar QueryFile = __webpack_require__(5).QueryFile;\nvar path = __webpack_require__(3);\n\nfunction sql(file) {\n    var fullPath = path.join(__dirname, file);\n    return new QueryFile(fullPath, { debug: true });\n}\n\nmodule.exports = {\n    geoFile: {\n        tableCreate: sql('./fctDataSQL/HIAA_LOAD_GEO_FACTORS.sql'),\n        insertGeoFactorData: sql('./fctDataSQL/INSERT_GEO_FACTORS.sql')\n    },\n    medFile: {\n        tableCreate: sql('./rvsDataSQL/HIAA_LOAD_MEDICAL_SERVICE.sql')\n    },\n    zipFile: {\n        tableCreate: sql('./zipDataSQL/HIAA_LOAD_ZIP_CODES.sql'),\n        insertZipCodes: sql('./zipDataSQL/INSERT_ZIP_CODES.sql')\n    }\n};\n/* WEBPACK VAR INJECTION */}.call(exports, \"sql\"))\n\n//////////////////\n// WEBPACK FOOTER\n// ./sql/sql.js\n// module id = 13\n// module chunks = 0\n\n//# sourceURL=webpack:///./sql/sql.js?");

/***/ })
/******/ ]);