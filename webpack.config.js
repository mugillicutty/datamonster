/**
 * Created by jwarnock on 2/3/17.
 */

var webpack = require('webpack');
var path = require('path');
var fs = require('fs');


var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

var webpackConfig = {
    watch: false,
    entry: ["babel-polyfill", path.resolve(__dirname, './src/index.js')],
    output: {
        path: path.resolve(__dirname, './public'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0']
                }
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /\.(json|map.json)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                loader: ['json-loader', 'json']
            },
            {
                test: /\.(jpe?g|jpeg|png|gif|svg|jpg)$/i,
                loaders: [
                    "file-loader?name=/static/img/[name].[ext]",
                    {
                        loader: 'img-loader',
                        options: {
                            enabled: process.env.NODE_ENV === 'production',
                            gifsicle: {
                                optimizationLevel: 7,
                                interlaced: false
                            },
                            mozjpeg: {
                                quality: 100,
                                progressive: false,
                                arithmetic: false
                            },
                            optipng: {
                                optimizationLevel: 7,
                                interlaced: false
                            },
                            pngquant: {
                                quality: '100',
                                speed: 4
                            },
                            svgo: {
                                plugins: [
                                    {removeTitle: true},
                                    {convertPathData: false},
                                    {removeViewBox: false},
                                    {removeEmptyAttrs: false}
                                ]
                            }
                        }
                    },
                ]
            },
            {
                test: /\.css$/,
                loader: [ 'style-loader', 'css-loader' ],
            }
        ]
    },
    // target: 'node',
    node: {
        fs: "empty",
        __filename: true,
        __dirname: true,
        global: true,
        process: true,
        console: true
    },
    externals: {
        'path': '{}',
        "fs": "{}",
        nodeModules
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            debug: true,
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                conditionals: true,
                loops: true,
                unused: true,
                join_vars: true,
                // drop_console: true
            },
            mangle: true,
            sourcemap: false,
            beautify: false,
            dead_code: true
        }),
    ],
    devtool: 'eval'
    // cheap-module-source-map
    // eval
    // source-map
};


module.exports = webpackConfig;