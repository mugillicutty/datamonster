var bluebird = require('bluebird');
var connectOptions = {
    promiseLib: bluebird,
    capSQL: true,
    // connect: (client, dc, isFresh) => {
        // const cp = client.connectionParameters;
        // console.log('Connected to database:', cp.database + ':' + cp.port);
        // console.log(client);
        // console.log(isFresh);
    // },
};
var pgp = require('pg-promise')(connectOptions);

// Connection configuration object to msttest pgpool
var config = {
    host: '192.168.1.48',
    port: '9999',
    database: 'edb',
    user: 'enterprisedb',
    password: '$010209!'
};

var productionHTTPS = {
    host: '192.168.1.40',
    port: '9999',
    database: 'edb',
    user: 'enterprisedb',
    password: '$010209!'
};

var productionConfig = {
    host: '192.168.1.40',
    port: '9999',
    database: 'edb',
    user: 'enterprisedb',
    password: '$010209!'
};

// Connection configuration object to local
var config2 = {
    host: 'localhost',
    port: '5432',
    database: 'postgres',
    user: 'postgres',
    password: 'josh1470'
};

var edbTest = pgp(config);
var edb = pgp(productionConfig);

const MyConn = {
    pgp: pgp,
    edbTest: edbTest,
    edb: edb,
};

module.exports = MyConn;