//EXPRESS SERVER
const express = require('express');
const app = express();
const router = express.Router();
const path = require('path');
const fs = require('fs');


import React from 'react';
// const React = require('react');
// we'll use this to render our app to an html string
import { renderToString } from 'react-dom/server';
// const renderToString = require('react-dom/server');
// and these to match the url to routes and then render
import { match, RouterContext } from 'react-router';
// const match = require('react-router/matchPath');
// const RouterContext = require('react-router/Route');
import routes from './src/routes.js';
// const routes = require('./src/routes');


//PARSER
let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//CONNECTIONS
let MyConn = require('./connections/posgres-connection.js');
MyConn.edb.query('select inet_server_port();')
    .then(function(result) {
        try {
            var dbPort = result[0].inet_server_port;
            console.log('Connected to database on port: ' + dbPort);
        } catch(err) {
            console.log(err);
        }
    })
    .catch(function(err) {
        console.log(err)
    });

app.use(express.static(path.join(__dirname, './public')));


//API'S *Require and use API'S here*

let dataMonsterAPI = require('./api/datamonster-api.js');
app.use('/api', dataMonsterAPI);

app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    next();
});

// React Router
app.get('*', (req, res) => {
    match({ routes: routes, location: req.url }, (err, redirect, props) => {

        // in here we can make some decisions all at once
        if (err) {
            // there was an error somewhere during route matching
            res.status(500).send(err.message)

        } else if (redirect) {
            // we haven't talked about `onEnter` hooks on routes, but before a
            // route is entered, it can redirect. Here we handle on the server.
            res.redirect(redirect.pathname + redirect.search)

        } else if (props) {
            // if we got props then we matched a route and can render
            const appHtml = renderToString(<RouterContext {...props}/>);
            res.send(renderPage(appHtml))

        } else {
            // no errors, no redirect, we just didn't match anything
            res.status(404).send('Not Found')
        }
    })
});

function renderPage(appHtml) {
    return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Myers-Stevens & Toohey & Co., Inc.</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/index.css">
    </head>
    <body>
    <div id=app>${appHtml}</div>
    <script src="./bundle.js"></script>
    </body>
    </html>
    `
}

//ROUTER FUNCTIONS
router.use(function(req, res, next) {
    console.log('Server ' + req.method, req.url, req.body);
    next();
});

app.use('/api', router);



//SERVER CONNECTION
var port = process.env.PORT || 9898;
var server = app.listen(process.env.PORT || port || 9898, function() {
    console.log('Server is running at https://localhost:' +
        server.address().port);
});
module.exports = app;