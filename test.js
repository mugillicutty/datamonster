


var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('./server.bundle');
var should = chai.should();

let requestURL = 'http://localhost:9898/api';

chai.use(chaiHttp);


describe('Conect Test', () => {
    it('should get stat 200', (done) => {
        chai.request(requestURL)
            .get('/connecttest')
            .end(function(err, res){
                res.should.have.status(200);
                done();
            });
    });
});