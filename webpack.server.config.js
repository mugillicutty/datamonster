/**
 * Created by jwarnock on 2/6/17.
 */

let webpack = require('webpack');
let fs = require('fs');
let path = require('path');

let webpackServerConfig = {
    watch: false,
    entry: path.resolve(__dirname, './server.js'),
    output: {
        path: path.resolve(__dirname),
        filename: './server.bundle.js'
    },
    target: 'node',
    externals: fs.readdirSync(path.resolve(__dirname, 'node_modules')).concat([
        'react-dom/server', 'react/addons',
    ]).reduce(function (ext, mod) {
        ext[mod] = 'commonjs ' + mod;
        return ext
    }, {}),
    node: {
        fs: "empty",
        __filename: true,
        __dirname: true,
        global: true,
        process: true,
        console: true
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'models')],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0']
                }
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /\.(json|map.json)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                loader: ['json-loader', 'json']
            },
            {
                test: /\.(jpe?g|jpeg|png|gif|svg|jpg)$/i,
                loaders: [
                    "file-loader?name=/static/img/[name].[ext]",
                    {
                        loader: 'img-loader',
                        options: {
                            enabled: process.env.NODE_ENV === 'production',
                            gifsicle: {
                                optimizationLevel: 7,
                                interlaced: false
                            },
                            mozjpeg: {
                                quality: 100,
                                progressive: false,
                                arithmetic: false
                            },
                            optipng: {
                                optimizationLevel: 7,
                                interlaced: false
                            },
                            pngquant: {
                                quality: '100',
                                speed: 4
                            },
                            svgo: {
                                plugins: [
                                    {removeTitle: true},
                                    {convertPathData: false},
                                    {removeViewBox: false},
                                    {removeEmptyAttrs: false}
                                ]
                            }
                        }
                    },
                ]
            },
            {
                test: /\.css$/,
                loader: [ 'style-loader', 'css-loader' ]
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            debug: true,
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         warnings: false,
        //         conditionals: true,
        //         loops: true,
        //         unused: true,
        //         join_vars: true,
        //         // drop_console: true
        //     },
        //     mangle: true,
        //     sourcemap: false,
        //     beautify: false,
        //     dead_code: true
        // }),

    ],
    devtool: 'eval'
    // cheap-module-source-map
    // eval
    // source-map
};

module.exports = webpackServerConfig;

