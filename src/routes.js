/**
 * Created by jwarnock on 2/3/17.
 */
import React from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'


module.exports = (

    <Router path="/" history={browserHistory}>
        <IndexRoute/>

    </Router>

);

