/**
 * Created by jwarnock on 7/28/17.
 */
const path = require('path');
const fs = require('fs');
const DatFunc = require('./DataParseFunc');
const rvsDAT = path.resolve(__dirname, '../../DAT/ucrrvs.dat');
const MyConn = require('../../connections/posgres-connection');

function Inserts(template, data) {
    if (!(this instanceof Inserts)) {
        return new Inserts(template, data);
    }
    this._rawDBType = true;
    this.formatDBType = function () {
        return data.map(d => '(' + MyConn.pgp.as.format(template, d) + ')').join();
    };
}

const RVSParse = {
    run: () => {
        return DatFunc.createMedicalServiceTable()
            .then(res => {
                const createTableResponse = {
                    status: res.status,
                    text: res.statusText,
                    data: res.data,
                };
                if (createTableResponse.data.code !== 1) {
                    return Promise.reject(createTableResponse);
                } else {
                    return DatFunc.rvsDataParse(rvsDAT)
                }
            })
            .then(res => {
                let data = res;
                let values = new Inserts(
                    '${procedureCode}, ' +
                    '${relativeValueIn} ', data);

                return MyConn.edb.none('INSERT INTO insrdata.hiaa_load_medical_services(' +
                    '            procedure_code,' +
                    '            relative_value_in)' +
                    '    VALUES $1',
                    values)
            })
            .then(function (data) {
                const msg = 'RVS Done!!';
                console.log('RVS Done!!');
                return msg;
            })
            .catch(err => {
                console.log(err);
            })
    },
    runTest: () => {
        return DatFunc.createTestMedicalServiceTable()
            .then(res => {
                const createTableResponse = {
                    status: res.status,
                    text: res.statusText,
                    data: res.data,
                };
                if (createTableResponse.data.code !== 1) {
                    return Promise.reject(createTableResponse);
                } else {
                    return DatFunc.rvsDataParse(rvsDAT)
                }
            })
            .then(res => {
                let data = res;
                let values = new Inserts(
                    '${procedureCode}, ' +
                    '${relativeValueIn} ', data);

                return MyConn.edbTest.none('INSERT INTO insrdata.hiaa_load_medical_services(' +
                    '            procedure_code,' +
                    '            relative_value_in)' +
                    '    VALUES $1',
                    values)
            })
            .then((data) => {
                const msg = 'RVS Done!!';
                console.log('RVS Done!!');
                return msg;
            })
            .catch(err => {
                console.log(err);
            })
    },
};

module.exports = RVSParse;