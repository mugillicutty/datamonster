/**
 * Created by jwarnock on 7/28/17.
 */

const path = require('path');
const fs = require('fs');
const DatFunc = require('./DataParseFunc');
const fctDAT = path.resolve(__dirname, '../../DAT/ucrfct.dat');
const smallFCTDAT = path.resolve(__dirname, '../../DAT/smallucrfct.dat');
var sql = require('../../sql/sql.js');
const axios = require('axios');
const myAxios = axios.create({
    baseURL: 'http://localhost:9898',
});
const MyConn = require('../../connections/posgres-connection');

function Inserts(template, data) {
    if (!(this instanceof Inserts)) {
        return new Inserts(template, data);
    }
    this._rawDBType = true;
    this.formatDBType = function () {
        return data.map(d => '(' + MyConn.pgp.as.format(template, d) + ')').join();
    };
}
const FCTParse = {
    run: () => {
        return DatFunc.createGeoFactorsTable()
            .then(res => {
                const createTableResponse = {
                    status: res.status,
                    text: res.statusText,
                    data: res.data,
                };
                if (createTableResponse.data.code !== 1) {
                    return Promise.reject(createTableResponse);
                } else {
                    return DatFunc.fctDataParse(fctDAT)
                }
            })
            .then(res => {
                let geoOne = res;
                DatFunc.fctPercentDataParse(fctDAT)
                    .then(res => {
                        let combObj = [];
                        geoOne.forEach((record, i) => {
                            const newObj = Object.assign(record, res[i]);
                            combObj.push(newObj);
                        });
                        return combObj;
                    })
                    .then(res => {
                        let data = res;
                        let values = new Inserts(
                            '${geoZipCode}, ' +
                            '${procedureCodeLow}, ' +
                            '${procedureCodeHigh}, ' +
                            'NULL, ' +
                            'NULL, ' +
                            'NULL, ' +
                            'NULL, ' +
                            '${percentile60in}, ' +
                            '${percentile80in}, ' +
                            '${percentile85in}, ' +
                            '${percentile90in}, ' +
                            '${conversionFactorType}', data);
                        MyConn.edb.none('INSERT INTO insrdata.hiaa_load_geo_factors(' +
                            '            geo_zip_code,' +
                            '            procedure_code_low,' +
                            '            procedure_code_high,' +
                            '            percentile_60,' +
                            '            percentile_80,' +
                            '            percentile_85,' +
                            '            percentile_90,' +
                            '            percentile_60_in,' +
                            '            percentile_80_in,' +
                            '            percentile_85_in,' +
                            '            percentile_90_in,' +
                            '            conversion_factor_type' +
                            ')' +
                            '    VALUES $1', values)
                            .then(function (data) {
                                const msg = 'FCT Done!!';
                                console.log('FCT Done!!');
                                return msg;
                            })
                            .catch(function (error) {
                                console.log(error)
                            })
                    })
            })
            .catch(err => {
                console.log(err);
            });
    },
    runTest: () => {
        return DatFunc.createTestGeoFactorsTable()
            .then(res => {
                const createTableResponse = {
                    status: res.status,
                    text: res.statusText,
                    data: res.data,
                };
                if (createTableResponse.data.code !== 1) {
                    return Promise.reject(createTableResponse);
                } else {
                    return DatFunc.fctDataParse(fctDAT)
                }
            })
            .then(res => {
                let geoOne = res;
                DatFunc.fctPercentDataParse(fctDAT)
                    .then(res => {
                        let combObj = [];
                        geoOne.forEach((record, i) => {
                            const newObj = Object.assign(record, res[i]);
                            combObj.push(newObj);
                        });
                        return combObj;
                    })
                    .then(res => {
                        let data = res;
                        let values = new Inserts(
                            '${geoZipCode}, ' +
                            '${procedureCodeLow}, ' +
                            '${procedureCodeHigh}, ' +
                            'NULL, ' +
                            'NULL, ' +
                            'NULL, ' +
                            'NULL, ' +
                            '${percentile60in}, ' +
                            '${percentile80in}, ' +
                            '${percentile85in}, ' +
                            '${percentile90in}, ' +
                            '${conversionFactorType}', data);
                        MyConn.edbTest.none('INSERT INTO insrdata.hiaa_load_geo_factors(' +
                            '            geo_zip_code,' +
                            '            procedure_code_low,' +
                            '            procedure_code_high,' +
                            '            percentile_60,' +
                            '            percentile_80,' +
                            '            percentile_85,' +
                            '            percentile_90,' +
                            '            percentile_60_in,' +
                            '            percentile_80_in,' +
                            '            percentile_85_in,' +
                            '            percentile_90_in,' +
                            '            conversion_factor_type' +
                            ')' +
                            '    VALUES $1', values)
                            .then((data) => {
                                const msg = 'FCT Done!!';
                                console.log('FCT Done!!');
                                return msg;
                            })
                            .catch((error) => {
                                console.log(error)
                            })
                    })
            })
            .catch(err => {
                console.log(err);
            });
    },
};
module.exports = FCTParse;