/**
 * Created by jwarnock on 7/27/17.
 */

"use strict";

const path = require('path');
const fs = require('fs');
const axios = require('axios');
const myAxios = axios.create({
    baseURL: 'http://localhost:9898',
});
const MyConn = require('../../connections/posgres-connection');

const DatFunc = {

    read: (file) => {
        return fs.readFileSync(file, {encoding: 'utf8', flag: 'r'})
    },

    chunk: (data, chunkSize) => {
        return data.match(new RegExp(`.{1,${chunkSize}}`, 'g'));
    },

    splitUp: (arr, n) => {
        let res = [];
        while (arr.length) {
            res.push(arr.splice(0, n));
        }
        return res;
    },

    // RVS MEDICAL SERVICE
    createMedicalServiceTable: () => {
        return myAxios.post('/api/medicalservicecreate')
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },
    createTestMedicalServiceTable: () => {
        return myAxios.post('/api/testmedicalservicecreate')
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },
    rvsDataParse: (data) => {
        return new Promise((resolve, reject) => {
            if (!data) {
                reject(new Error('Parameters needed'));
            } else {
                const readData = DatFunc.read(data),
                    chunkData = DatFunc.chunk(readData, 19);
                let rowData = [];
                chunkData.forEach(record => {

                    rowData.push({
                        procedureCode: record.slice(3,8).trim(),
                        relativeValueIn: record.slice(8,15).trim(),
                    })
                });
                resolve(rowData);
            }
        })
    },

    // ZIP ZIP CODE
    createZipCodeTable: () => {
        return myAxios.post('/api/zipcodecreate')
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },
    createTestZipCodeTable: () => {
        return myAxios.post('/api/testzipcodecreate')
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },
    zipDataParse: (data) => {
        return new Promise((resolve, reject) => {
            if (!data) {
                reject(new Error('Parameters needed'));
            } else {
                const readData = DatFunc.read(data),
                    chunkData = DatFunc.chunk(readData, 93);
                let rowData = [];
                chunkData.forEach(record => {
                    rowData.push({
                        zipCodeLow: record.slice(0,3).trim(),
                        zipCodeHigh: record.slice(3,6).trim(),
                        geoZipCode: record.slice(6,9).trim(),
                    })
                });
                resolve(rowData);
            }
        })
    },
    insertZipCodeData: (data) => {
        return myAxios.post('/api/insertzipcodes', {data})
            .then(res => {
                return res;
            })
            .catch(err => {
                console.log(err);
                return err;
            })
    },

    // FCT GEO FACTOR
    createGeoFactorsTable: () => {
        return myAxios.post('/api/geofactorscreate')
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },
    createTestGeoFactorsTable: () => {
        return myAxios.post('/api/testgeofactorscreate')
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },
    fctDataParse: (data) => {
        return new Promise((resolve, reject) => {
            if (!data) {
                reject(new Error('Parameters needed'));
            } else {
                const readData = DatFunc.read(data),
                    chunkData = DatFunc.chunk(readData, 92);
                let rowData = [];
                for (let i = 0, chunkLength = chunkData.length; i < chunkLength; i++) {
                    rowData.push({
                        geoZipCode: chunkData[i].slice(0,3),
                        conversionFactorType: chunkData[i].slice(3,5),
                        procedureCodeLow: chunkData[i].slice(5, 10),
                        procedureCodeHigh: chunkData[i].slice(10,15),
                    })
                }
                resolve(rowData);
            }
        })
    },
    fctPercentDataParse: (data) => {
        return new Promise((resolve, reject) => {
            if (!data) {
                reject(new Error('Parameters needed'));
            } else {
                const readData = DatFunc.read(data),
                    chunkData = DatFunc.chunk(readData, 92);
                let rowData = [];
                for (let i = 0, chunkLength = chunkData.length; i < chunkLength; i++) {
                    rowData.push({
                        percentile60in: chunkData[i].slice(22,29),
                        percentile80in: chunkData[i].slice(43,50),
                        percentile85in: chunkData[i].slice(50,57),
                        percentile90in: chunkData[i].slice(64,71),
                    })
                }
                resolve(rowData);
            }
        })
    },
    // INSERT FUNCTIONS
    insertGeoFactorData: (data) => {
        return myAxios.post('/api/insertgeofactordata', {data})
            .then(res => {
                return res;
            })
            .catch(err => {
                return err;
            })
    },

};

module.exports = DatFunc;


