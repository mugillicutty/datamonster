
const RVSParse = require('./rvs_parse');
const FCTParse = require('./fct_parse');
const ZIPParse = require('./zip_parse');

// runFairHealth = () => {
//     return RVSParse.run()
//         .then(res => {
//             console.log('RVS PARSED');
//             return ZIPParse.run()
//         })
//         .then(res => {
//             console.log('ZIP PARSED');
//             return FCTParse.run();
//         })
//         .then(res => {
//             console.log('FCT PARSED');
//         })
//         .catch(err => {
//             console.log(err);
//         });
// };
//
// runFairHealth();

runFairHealthTest = () => {
    return RVSParse.runTest()
        .then(res => {
            console.log('RVS PARSED');
            return ZIPParse.runTest()
        })
        .then(res => {
            console.log('ZIP PARSED');
            return FCTParse.runTest();
        })
        .then(res => {
            console.log('FCT PARSED');
        })
        .catch(err => {
            console.log(err);
        });
};

runFairHealthTest();