/**
 * Created by jwarnock on 7/27/17.
 */

const path = require('path');
const fs = require('fs');
const DatFunc = require('./DataParseFunc');
const zipDAT = path.resolve(__dirname, '../../DAT/ucrzip.dat');
const MyConn = require('../../connections/posgres-connection');

 function Inserts(template, data) {
     if (!(this instanceof Inserts)) {
         return new Inserts(template, data);
     }
     this._rawDBType = true;
     this.formatDBType = function () {
         return data.map(d => '(' + MyConn.pgp.as.format(template, d) + ')').join();
     };
 }

 const ZIPParse = {
     run: () => {
         return DatFunc.createZipCodeTable()
             .then(res => {
                 const createTableResponse = {
                     status: res.status,
                     text: res.statusText,
                     data: res.data,
                 };
                 if (createTableResponse.data.code !== 1) {
                     return Promise.reject(createTableResponse);
                 } else {
                     return DatFunc.zipDataParse(zipDAT)
                 }
             })
             .then(res => {
                 console.log(res);
                 let data = res;

                 let values = new Inserts(
                     '${geoZipCode}, ' +
                     '${zipCodeLow}, ' +
                     '${zipCodeHigh}', data);

                 return MyConn.edb.none('INSERT INTO insrdata.hiaa_load_zip_codes(' +
                     '            geo_zip_code,' +
                     '            zip_code_low,' +
                     '            zip_code_high)' +
                     '    VALUES $1', values)
             })
             .then(function (data) {
                 const msg = 'ZIP Done!!';
                 console.log('ZIP Done!!');
                 return msg;
             })
             .catch(err => {
                 console.log(err);
             });
     },
     runTest: () => {
         return DatFunc.createTestZipCodeTable()
             .then(res => {
                 const createTableResponse = {
                     status: res.status,
                     text: res.statusText,
                     data: res.data,
                 };
                 if (createTableResponse.data.code !== 1) {
                     return Promise.reject(createTableResponse);
                 } else {
                     return DatFunc.zipDataParse(zipDAT)
                 }
             })
             .then(res => {
                 let data = res;
                 let values = new Inserts(
                     '${geoZipCode}, ' +
                     '${zipCodeLow}, ' +
                     '${zipCodeHigh}', data);

                 return MyConn.edbTest.none('INSERT INTO insrdata.hiaa_load_zip_codes(' +
                     '            geo_zip_code,' +
                     '            zip_code_low,' +
                     '            zip_code_high)' +
                     '    VALUES $1', values)
             })
             .then((data) => {
                 const msg = 'ZIP Done!!';
                 console.log('ZIP Done!!');
                 return msg;
             })
             .catch(err => {
                 console.log(err);
             });
     },
 };

module.exports = ZIPParse;