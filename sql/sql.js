var QueryFile = require('pg-promise').QueryFile;
var path = require('path');

function sql(file) {
    var fullPath = path.join(__dirname, file);
    return new QueryFile(fullPath, {debug: true});
}

module.exports = {
    geoFile: {
        tableCreate: sql('./fctDataSQL/HIAA_LOAD_GEO_FACTORS.sql'),
        insertGeoFactorData: sql('./fctDataSQL/INSERT_GEO_FACTORS.sql'),
    },
    medFile: {
        tableCreate: sql('./rvsDataSQL/HIAA_LOAD_MEDICAL_SERVICE.sql'),
    },
    zipFile: {
        tableCreate: sql('./zipDataSQL/HIAA_LOAD_ZIP_CODES.sql'),
        insertZipCodes: sql('./zipDataSQL/INSERT_ZIP_CODES.sql'),
    }
};
