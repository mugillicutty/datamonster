
CREATE TABLE IF NOT EXISTS insrdata.hiaa_load_geo_factors
(
  geo_zip_code character(4),
  procedure_code_low character(5),
  procedure_code_high character(5),
  percentile_60 numeric(7,3),
  percentile_80 numeric(7,3),
  percentile_85 numeric(7,3),
  percentile_90 numeric(7,3),
  percentile_60_in numeric(7,0),
  percentile_80_in numeric(7,0),
  percentile_85_in numeric(7,0),
  percentile_90_in numeric(7,0),
  conversion_factor_type character(2)
);

ALTER TABLE insrdata.hiaa_load_geo_factors
  OWNER TO enterprisedb;

GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO enterprisedb;
GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO insrdata;
GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO jwarnock;
GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO mgrant;
GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO pdtoohey;
GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO insrdata_user;
GRANT ALL ON TABLE insrdata.hiaa_load_geo_factors TO insrdata_reports;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO enterprisedb;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO insrdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO jwarnock;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO mgrant;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO pdtoohey;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO insrdata_user;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_geo_factors TO insrdata_reports;

