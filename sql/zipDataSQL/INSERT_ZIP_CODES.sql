INSERT INTO insrdata.hiaa_load_zip_codes
(
            geo_zip_code,
            zip_code_low,
            zip_code_high
)
    VALUES
(
    ${geoZipCode},
    ${zipCodeLow},
    ${zipCodeHigh}
);