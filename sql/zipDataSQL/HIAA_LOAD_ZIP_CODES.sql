CREATE TABLE IF NOT EXISTS insrdata.hiaa_load_zip_codes
(
  geo_zip_code character(3),
  zip_code_low character(3),
  zip_code_high character(3)
);

ALTER TABLE insrdata.hiaa_load_zip_codes
  OWNER TO enterprisedb;

GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO enterprisedb;
GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO insrdata;
GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO jwarnock;
GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO mgrant;
GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO pdtoohey;
GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO insrdata_user;
GRANT ALL ON TABLE insrdata.hiaa_load_zip_codes TO insrdata_reports;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO enterprisedb;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO insrdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO jwarnock;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO mgrant;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO pdtoohey;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO insrdata_user;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_zip_codes TO insrdata_reports;

CREATE INDEX IF NOT EXISTS ix_geo_zip_code
  ON insrdata.hiaa_load_zip_codes
  USING btree
  (geo_zip_code COLLATE pg_catalog."default");

CREATE INDEX IF NOT EXISTS ix_zip_code_high
  ON insrdata.hiaa_load_zip_codes
  USING btree
  (zip_code_high COLLATE pg_catalog."default");

CREATE INDEX IF NOT EXISTS ix_zip_code_low
  ON insrdata.hiaa_load_zip_codes
  USING btree
  (zip_code_low COLLATE pg_catalog."default");

