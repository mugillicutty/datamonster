
CREATE TABLE IF NOT EXISTS insrdata.hiaa_load_medical_services
(
  procedure_code character(5),
  relative_value_new numeric(7,2),
  medical_service_id numeric(8,0),
  relative_value_in numeric(7,0),
  service_in_hiaa_table character(1)
);

ALTER TABLE insrdata.hiaa_load_medical_services
  OWNER TO enterprisedb;

GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO enterprisedb;
GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO insrdata;
GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO jwarnock;
GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO mgrant;
GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO pdtoohey;
GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO insrdata_user;
GRANT ALL ON TABLE insrdata.hiaa_load_medical_services TO insrdata_reports;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO enterprisedb;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO insrdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO jwarnock;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO mgrant;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO pdtoohey;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO insrdata_user;
GRANT SELECT, UPDATE, INSERT, DELETE, TRIGGER ON TABLE insrdata.hiaa_load_medical_services TO insrdata_reports;

CREATE INDEX IF NOT EXISTS ix_hiaa_load_med_proc_code
  ON insrdata.hiaa_load_medical_services
  USING btree
  (procedure_code COLLATE pg_catalog."default");

CREATE INDEX IF NOT EXISTS ix_hiaa_load_med_rel_value
  ON insrdata.hiaa_load_medical_services
  USING btree
  (medical_service_id);
