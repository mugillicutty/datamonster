/**
 * Created by jwarnock on 7/31/17.
 */
var express = require('express');
var router = express.Router();
var MyConn = require('../connections/posgres-connection.js');
var sql = require('../sql/sql.js');


router.route('/connecttest')
    .get(function(req, res) {

        res.send('WORKING');

    });


// RVS MEDICAL SERVICE
// TABLE CREATE
router.route('/medicalservicecreate')
    .post(function(req, res) {
        MyConn.edb.query(sql.medFile.tableCreate)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Medical Service Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Medical Service Table Not Created',
                };
                res.send(tableCreateError);
            })
    });
// TEST
router.route('/testmedicalservicecreate')
    .post(function(req, res) {
        MyConn.edbTest.query(sql.medFile.tableCreate)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Medical Service Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Medical Service Table Not Created',
                };
                res.send(tableCreateError);
            })
    });

// ZIP ZIPCODE
// TABLE CREATE
router.route('/zipcodecreate')
    .post(function(req, res) {
        MyConn.edb.query(sql.zipFile.tableCreate)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Zip Codes Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Zip Codes Table Not Created',
                };
                res.send(tableCreateError);
            })
    });
// TEST
router.route('/testzipcodecreate')
    .post(function(req, res) {
        MyConn.edbTest.query(sql.zipFile.tableCreate)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Zip Codes Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Zip Codes Table Not Created',
                };
                res.send(tableCreateError);
            })
    });

// DATA INSERT
router.route('/insertzipcodes')
    .post(function(req, res) {
        const zipCodeData = {
            geoZipCode: req.body.geoZipCode,
            zipCodeLow: req.body.zipCodeLow,
            zipCodeHigh: req.body.zipCodeHigh,
        };

        MyConn.edb.query(sql.zipFile.insertZipCodes, zipCodeData)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Zip Codes Inserted',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Zip Codes Not Inserted',
                };
                res.send(tableCreateError);
            })
    });
// TEST
router.route('/testinsertzipcodes')
    .post(function(req, res) {
        const zipCodeData = {
            geoZipCode: req.body.geoZipCode,
            zipCodeLow: req.body.zipCodeLow,
            zipCodeHigh: req.body.zipCodeHigh,
        };
        MyConn.edbTest.query(sql.zipFile.insertZipCodes, zipCodeData)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Zip Codes Inserted',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Zip Codes Not Inserted',
                };
                res.send(tableCreateError);
            })
    });

// FCT GEO FACTOR

// TABLE CREATE
router.route('/geofactorscreate')
    .post(function(req, res) {
        MyConn.edb.query(sql.geoFile.tableCreate)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Geo Factors Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Geo Factors Table Not Created',
                };
                res.send(tableCreateError);
            })
    });
// TEST
router.route('/testgeofactorscreate')
    .post(function(req, res) {
        MyConn.edbTest.query(sql.geoFile.tableCreate)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Geo Factors Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Geo Factors Table Not Created',
                };
                res.send(tableCreateError);
            })
    });

// INSERT DATA
router.route('/insertgeofactordata')
    .post(function(req, res) {
        const geoFactorData = {
            geoZipCode: req.body.data.geoZipCode,
            procedureCodeLow: req.body.data.procedureCodeLow,
            procedureCodeHigh: req.body.data.procedureCodeHigh,
            percentile60in: req.body.data.percentile60in,
            percentile80in: req.body.data.percentile80in,
            percentile85in: req.body.data.percentile85in,
            percentile90in: req.body.data.percentile90in,
            conversionFactorType: req.body.data.conversionFactorType,
        };
        MyConn.edb.query(sql.geoFile.insertGeoFactorData, geoFactorData)
            .then(result => {
                const tableCreateResult = {
                    code: 1,
                    result: 'OK',
                    data: result,
                    message: 'Zip Codes Table Created',
                };
                res.send(tableCreateResult);
            })
            .catch(err => {
                console.log(err);
                const tableCreateError = {
                    code: 0,
                    result: 'Error',
                    data: err,
                    message: 'Zip Codes Table Not Created',
                };
                res.send(tableCreateError);
            })
    });

// router.route('/insertgeofactordatamass')
//     .post(function(req, res) {
//         const geoFactorData = {
//             geoZipCode: req.body.data.geoZipCode,
//             procedureCodeLow: req.body.data.procedureCodeLow,
//             procedureCodeHigh: req.body.data.procedureCodeHigh,
//             percentile60in: req.body.data.percentile60in,
//             percentile80in: req.body.data.percentile80in,
//             percentile85in: req.body.data.percentile85in,
//             percentile90in: req.body.data.percentile90in,
//             conversionFactorType: req.body.data.conversionFactorType,
//         };
//         const cs = new MyConn.pgp.helpers.ColumnSet([
//             {name: 'geo_zip_code', prop: 'geoZipCode'},
//             {name: 'procedure_code_low', prop: 'procedureCodeLow'},
//             {name: 'procedure_code_high', prop: 'procedureCodeHigh'},
//             {name: 'percentile_60_in', prop: 'percentile60in'},
//             {name: 'percentile_80_in', prop: 'percentile80in'},
//             {name: 'percentile_85_in', prop: 'percentile85in'},
//             {name: 'percentile_90_in', prop: 'percentile90in'},
//             {name: 'conversion_factor_type', prop: 'conversionFactorType'},
//         ]);
//         edb.query(sql.geoFile.insertGeoFactorData, geoFactorData)
//             .then(result => {
//                 // console.log(result);
//                 const tableCreateResult = {
//                     code: 1,
//                     result: 'OK',
//                     data: result,
//                     message: 'Zip Codes Table Created',
//                 };
//                 res.send(tableCreateResult);
//             })
//             .catch(err => {
//                 console.log(err);
//                 const tableCreateError = {
//                     code: 0,
//                     result: 'Error',
//                     data: err,
//                     message: 'Zip Codes Table Not Created',
//                 };
//                 res.send(tableCreateError);
//             })
//     });


module.exports = router;